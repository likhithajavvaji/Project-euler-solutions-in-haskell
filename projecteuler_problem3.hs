isFactor n x = rem n x ==0
isqrt = floor . sqrt. fromIntegral
isComposite n = or $ map (isFactor n)[2..isqrt n]
primeFactors n = last [x | x<-[2..isqrt n], rem n x == 0, not (isComposite x)]

main = putStrLn $ show (primeFactors 600851475143)
