import Numeric
import Data.char

palindrome xs = xs == reverse xs

solution = sum [x | x <- [1,3..999999], palindrome (show x)
                                        palindrome (showIntAtBase 2 intToDigit x "")]

main = do
       putStrLn $ show solution