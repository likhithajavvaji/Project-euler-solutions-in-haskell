import Data.Char

sumOfDigits = sum(map digitToInt n)
     where n = show(product[1..5])

main = print(sumOfDigits)