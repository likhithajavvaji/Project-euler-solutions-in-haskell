primes :: [Integer]
primes = listPrimes[2..]
   where 
      listPrimes :: [Integer] -> [Integer]
      listPrimes (a:xs) = a : listPrimes[x | x <- xs, x `rem` a > 0]

main = do
print(primes !! 10000)