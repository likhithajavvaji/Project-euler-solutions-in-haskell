import Data.List
import Data.Function

primes = 2:filter isPrime [3,5..]
isPrime n | n <= 0    = False
          | otherwise = null . tail . primeFactors $ n
primeFactors n = factor n primes
    where
        factor n (p:ps)
            | p * p > n    = [n]
            | mod n p == 0 = p:factor (div n p) (p:ps)
            | otherwise    = factor n ps


genPrimes a b = takeWhile isPrime . map (f a b) $ [0..]
   where
    f a b n = n * (n + a) + b


pairs = [((a,b), genPrimes a b) | b <- takeWhile (<1000) primes,
                                  f1 <- takeWhile (<1000+b+1) primes,
                                  let a = f1 - b - 1,
                                  isPrime (40 * (40 + a) + b)]

solution = (\((a,b),p) -> a*b) . maximumBy (compare `on` (length . snd)) $ pairs

main = do
    putStr "Solution: "
    putStrLn $ show solution