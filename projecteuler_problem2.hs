do { 
     problem2 = sum[x | x <- takeWhile (<= 1000000) fibs,even x] 
           where fibs = 0: 1: zipWith (+) fibs(tail fibs) 
main = putStrLn (show problem2) }    