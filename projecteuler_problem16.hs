import Data.Char

sumOfDigits = sum(map digitToInt n)
    where n = show(2^1000)
main = print(sumOfDigits)
