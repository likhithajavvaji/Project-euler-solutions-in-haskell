import Data.Char

solution = sum [x | x <- [10..6*9^5], f x == x]
  where
     f = sum . map ((^5) . digitToInt) . show

main = do
    putStr "Solution: "
    putStrLn $ show solution