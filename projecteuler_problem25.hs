import Data.List
import Data.Maybe

fib = 0:1:next fib
    where
        next (a:b:xs) = (a+b):next (b:xs)

solution = fromJust . findIndex (>10^999 - 1) $ fib

main = do
    putStr "Solution: "
    putStrLn $ show solution