import Data.Char

digits = map digitToInt . show

solution = sum [x | x <- [10..100000], curious x]
  where
    curious x = (==x) . sum . map (factorials!!) . digits $ x
    factorials = 1:scanl1 (*) [1..]

main = do
   putStr "Solution: "
   putStrLn $ show solution