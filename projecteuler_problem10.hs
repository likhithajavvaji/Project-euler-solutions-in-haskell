let isFactor n x = rem n x ==0

let isqrt = floor . sqrt. fromIntegral

let isComposite n = or $ map (isFactor n)[2..isqrt n]

let primeFactors n =[x | x<-[2..isqrt n], rem n x == 0, not (isComposite x)]


primes = 2 : filter(null.tail.primeFactors)[3,5..]
problem10 =  sum(takeWhile (< 1000000) primes)
main = putStrLn(show problem10)